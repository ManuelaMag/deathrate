import React, {useContext} from 'react';
import './App.css';
import DropDown from "./components/DropDown";
import AppContext from "./context/AppContext";
import MaterialTableDemo from "./components/MaterialTableDemo";
import Header from "./components/Header";


function App() {
    const {state} = useContext(AppContext);
    const {year} = state;
    if (year === '')
        return (
            <div>
                <Header/>
                <br/>
                <DropDown/>
            </div>
        );
    else {
        return (
            <div className="App">
                <Header/>
                <br/>
                <DropDown/>
                <br/>
                <div>
                    <MaterialTableDemo className="table"/>
                </div>
            </div>
        );
    }
}

export default App;
