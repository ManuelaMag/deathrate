import * as PropTypes from "prop-types";
import React, {useReducer} from "react";
import {Provider} from './AppContext';
import reducer from './Reducer';

const initialState = {
    year: '',
    deathRates: {
        loading: true,
        error: null,
        data: []
    },
    createDeathRate: {
        createLoading: true,
        createError: null,
        createData: []
    },
    updateDeathRate: {
        updateLoading: true,
        updateError: null,
        updateData: []
    },
};

const AppProvider = (props) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    return (
        <Provider value={{
            state,
            dispatch
        }}>
            {props.children}
        </Provider>
    );
};
AppProvider.propTypes = {
    children: PropTypes.node,
};


export default AppProvider;