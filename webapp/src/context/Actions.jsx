export const URL_API = 'http://localhost:8080'

export const CHANGE_YEAR = 'CHANGE_YEAR';

export function changeYear(newYear) {
    return {
        type: CHANGE_YEAR,
        payload: {
            data: newYear
        },
    }
}

export const FETCH_DEATHRATES_STARTED = 'FETCH_DEATHRATES_STARTED';
export const FETCH_DEATHRATES_SUCCESS = 'FETCH_DEATHRATES_SUCCESS';
export const FETCH_DEATHRATES_FAILURE = 'FETCH_DEATHRATES_FAILURE';

export function fetchDeathRatesStarted() {
    return {
        type: FETCH_DEATHRATES_STARTED,
    }
}

export function fetchDeathRatesSuccess(deathRates) {
    return {
        type: FETCH_DEATHRATES_SUCCESS,
        payload: {
            data: deathRates
        }
    }
}

export function fetchDeathRatesFailure(message) {
    return {
        type: FETCH_DEATHRATES_FAILURE,
        payload: {
            error: message
        }
    }
}

export const FETCH_CREATE_DEATHRATE_STARTED = 'FETCH_CREATE_DEATHRATE_STARTED';
export const FETCH_CREATE_DEATHRATE_SUCCESS = 'FETCH_CREATE_DEATHRATE_SUCCESS';
export const FETCH_CREATE_DEATHRATE_FAILURE = 'FETCH_CREATE_DEATHRATE_FAILURE';

export function fetchCreateDeathRateStarted() {
    return {
        type: FETCH_CREATE_DEATHRATE_STARTED,
    }
}

export function fetchCreateDeathRateSuccess(deathRate) {
    return {
        type: FETCH_CREATE_DEATHRATE_SUCCESS,
        payload: {
            data: deathRate
        }
    }
}

export function fetchCreateDeathRateFailure(message) {
    return {
        type: FETCH_CREATE_DEATHRATE_FAILURE,
        payload: {
            error: message
        }
    }
}

export const FETCH_UPDATE_DEATHRATE_STARTED = 'FETCH_UPDATE_DEATHRATE_STARTED';
export const FETCH_UPDATE_DEATHRATE_SUCCESS = 'FETCH_UPDATE_DEATHRATE_SUCCESS';
export const FETCH_UPDATE_DEATHRATE_FAILURE = 'FETCH_UPDATE_DEATHRATE_FAILURE';

export function fetchUpdateDeathRateStarted() {
    return {
        type: FETCH_UPDATE_DEATHRATE_STARTED,
    }
}

export function fetchUpdateDeathRateSuccess(deathRate) {
    return {
        type: FETCH_UPDATE_DEATHRATE_SUCCESS,
        payload: {
            data: deathRate
        }
    }
}

export function fetchUpdateDeathRateFailure(message) {
    return {
        type: FETCH_UPDATE_DEATHRATE_FAILURE,
        payload: {
            error: message
        }
    }
}
