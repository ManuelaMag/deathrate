import {
    CHANGE_YEAR, FETCH_CREATE_DEATHRATE_FAILURE, FETCH_CREATE_DEATHRATE_STARTED, FETCH_CREATE_DEATHRATE_SUCCESS,
    FETCH_DEATHRATES_FAILURE,
    FETCH_DEATHRATES_STARTED,
    FETCH_DEATHRATES_SUCCESS,
    FETCH_UPDATE_DEATHRATE_FAILURE,
    FETCH_UPDATE_DEATHRATE_STARTED,
    FETCH_UPDATE_DEATHRATE_SUCCESS
} from "./Actions";

function reducer(state, action) {
    switch (action.type) {
        case CHANGE_YEAR:
            return {
                ...state,
                year: action.payload.data,
            }
        case FETCH_DEATHRATES_STARTED:
            return {
                ...state,
                deathRates: {
                    loading: true,
                    error: null,
                    data: []
                }
            }
        case FETCH_DEATHRATES_SUCCESS:
            return {
                ...state,
                deathRates: {
                    loading: false,
                    error: null,
                    data: action.payload.data.deathRateList
                }
            }
        case FETCH_DEATHRATES_FAILURE:
            return {
                ...state,
                deathRates: {
                    loading: false,
                    error: action.payload.error,
                    data: []
                }
            }
                ;
            ;
        case FETCH_CREATE_DEATHRATE_STARTED:
            return {
                ...state,
                createDeathRate: {
                    createLoading: true,
                    createError: null,
                    createData: []
                }
            }
        case FETCH_CREATE_DEATHRATE_SUCCESS:
            return {
                ...state,
                createDeathRate: {
                    createLoading: false,
                    createError: null,
                    createData: action.payload.data
                }
            }
        case FETCH_CREATE_DEATHRATE_FAILURE:
            return {
                ...state,
                createDeathRate: {
                    createLoading: false,
                    createError: action.payload.error,
                    createData: []
                }
            }
        case FETCH_UPDATE_DEATHRATE_STARTED:
            return {
                ...state,
                updateDeathRate: {
                    updateLoading: true,
                    updateError: null,
                    updateData: []
                }
            }
        case FETCH_UPDATE_DEATHRATE_SUCCESS:
            return {
                ...state,
                updateDeathRate: {
                    updateLoading: false,
                    updateError: null,
                    updateData: action.payload.data
                }
            }
        case FETCH_UPDATE_DEATHRATE_FAILURE:
            return {
                ...state,
                updateDeathRate: {
                    updateLoading: false,
                    updateError: action.payload.error,
                    updateData: []
                }
            }
        default:
            return state;
    }
}

export default reducer;
