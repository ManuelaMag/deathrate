import React, {useContext, useState} from "react";
import Input from '@material-ui/core/Input';
import FormControl from "@material-ui/core/FormControl";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import CreateDeathRate from "./CreateDeathRate";
import AppContext from "../context/AppContext";

const FormCreateDeathRate = () => {

    const {state, dispatch} = useContext(AppContext);
    const {year} = state;

    let [country, setCountry] = useState('')
    let [femaleRate, setFemaleRate] = useState('')
    let [maleRate, setMaleRate] = useState('')


    const useStyles = makeStyles((theme) => ({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 200,
        },
        root: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        margin: {
            margin: theme.spacing(1),
        },
        withoutLabel: {
            marginTop: theme.spacing(3),
        },
    }));

    const classes = useStyles();


    return (
        <div>
            <form>
                <fieldset>
                    <legend>Add new death rate</legend>
                    <div>
                        <div>
                            <FormControl fullWidth className={classes.margin}>
                                <Input
                                    id="value"
                                    type={"text"}
                                    minlength="2"
                                    maxlength="2"
                                    required={true}
                                    onChange={event => setCountry(event.target.value)}
                                    style={{width: "400px", height: "30px"}}
                                    placeholder="Country*"
                                />
                            </FormControl>

                            <br/>
                            <br/>
                            <FormControl fullWidth className={classes.margin}>
                                <Input
                                    id="value"
                                    type={"number"}
                                    required={true}
                                    min="0.00"
                                    max="1000.00"
                                    onChange={event => setFemaleRate(event.target.value)}
                                    style={{width: "400px", height: "30px"}}
                                    placeholder="Female Rate*"
                                />
                            </FormControl>
                            <br/>
                            <br/>
                            <FormControl fullWidth className={classes.margin}>
                                <Input
                                    id="value"
                                    type={"number"}
                                    required={true}
                                    min="0.00"
                                    max="1000.00"
                                    onChange={event => setMaleRate(event.target.value)}
                                    style={{width: "400px", height: "30px"}}
                                    placeholder="Male Rate*"
                                />
                            </FormControl>
                            <br/>
                            <br/>
                            <div className="row">
                                <Button
                                    type="submit"
                                    style={{width: "400px", height: "40px", backgroundColor: "lightgrey"}}
                                    onClick={() => CreateDeathRate(year, country, femaleRate, maleRate, state, dispatch)}
                                >Add Death Rate
                                </Button>
                                <br/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
            <br/>
        </div>
    );
};

export default FormCreateDeathRate;