import {
    fetchCreateDeathRateFailure,
    fetchCreateDeathRateStarted,
    fetchCreateDeathRateSuccess,
    URL_API
} from "../context/Actions";

function CreateDeathRate(year, country, femaleRate, maleRate, state, dispatch) {
    if (country === '' || femaleRate === "" || maleRate === '') return alert("Unfilled fields")

    function json(response) {
        return response.json()
    }

    dispatch(fetchCreateDeathRateStarted(state));

    fetch(`${URL_API}/deathrates/${year}/countries`, {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify(
            {
                country: country,
                femaleRate: femaleRate,
                maleRate: maleRate
            }
        )
    })
        .then(json)
        .then(function (data) {
            if (data.year !== undefined) {
                return alert("Death rate created with success")
            } else {
                return alert("Death rate didn't create: " + data.details)
            }
        })
        .then(data => {
            return (dispatch(fetchCreateDeathRateSuccess(data))
            )
        })
        .catch(function (error) {
            alert("Error: " + error.message)
        })
        .catch(error => {
            return dispatch(fetchCreateDeathRateFailure(error.message))
        })


}

export default CreateDeathRate;