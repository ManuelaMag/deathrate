import {
    fetchUpdateDeathRateFailure,
    fetchUpdateDeathRateStarted,
    fetchUpdateDeathRateSuccess,
    URL_API
} from "../context/Actions";

function UpdateDeathRate(year, country, femaleRate, maleRate, state, dispatch) {

    function json(response) {
        return response.json()
    }

    dispatch(fetchUpdateDeathRateStarted(state));

    fetch(`${URL_API}/deathrates/${year}/countries/${country}`, {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify(
            {
                femaleRate: femaleRate,
                maleRate: maleRate
            }
        )
    })
        .then(json)
        .then(function (data) {
            if (data.year !== undefined) {
                return alert("Death rate updated with success")
            } else {
                return alert("Death rate didn't update: " + data.details)
            }
        })
        .then(data => {
                return dispatch(fetchUpdateDeathRateSuccess(data))
            }
        )
        .catch(function (error) {
            alert("Error: " + error.message)
        })
        .catch(error => {
            return dispatch(fetchUpdateDeathRateFailure(error.message))
        })
}

export default UpdateDeathRate;