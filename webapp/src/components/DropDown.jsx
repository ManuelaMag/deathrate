import React, {useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {changeYear} from "../context/Actions";
import AppContext from "../context/AppContext";

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export default function SimpleSelect() {
    const {dispatch} = useContext(AppContext);
    const classes = useStyles();
    const [year, setYear] = React.useState('');

    const handleChange = (event) => {
        setYear(event.target.value);
        dispatch(changeYear(event.target.value))
    };
    const actualYear = (new Date()).getFullYear();
    const years = Array.from(new Array(20), (val, index) => actualYear - index);
    return (
        <div>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Year</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={year}
                    onChange={handleChange}
                >
                    {
                        years.map((year, index) => {
                            return <MenuItem value={year}>{year}</MenuItem>
                        })
                    }
                </Select>
            </FormControl>

        </div>
    );
}
