import React, {Fragment, useContext, useEffect} from 'react';
import MaterialTable from 'material-table';
import AppContext from "../context/AppContext";
import {fetchDeathRatesFailure, fetchDeathRatesStarted, fetchDeathRatesSuccess, URL_API} from "../context/Actions";
import UpdateDeathRate from "./UpdateDeathRate";
import CreateDeathRate from "./CreateDeathRate";
import CircularIndeterminate from "./CircularIndeterminate";
import FormCreateDeathRate from "./FormCreateDeathRate";

export default function MaterialTableDemo() {
    const {state, dispatch} = useContext(AppContext);
    const {year, deathRates, createDeathRate, updateDeathRate} = state;
    const {loading, data, error} = deathRates;
    const [newState] = React.useState({
        columns: [
            {title: 'Country', field: 'country'},
            {title: 'Female Rate', field: 'femaleRate', type: 'numeric'},
            {title: 'Male Rate', field: 'maleRate', type: 'numeric'},
        ],
        data: [
            {
                country: '',
                femaleRate: '',
                maleRate: ''
            }
        ]
    });
    useEffect(() => {
        dispatch(fetchDeathRatesStarted(state));
        fetch(`${URL_API}/deathrates/${year}`)
            .then(res => res.json())
            .then(res => {
                    return dispatch(fetchDeathRatesSuccess(res))
                }
            )
            .catch(err => {
                return dispatch(fetchDeathRatesFailure(err.message))
            })

        ;
    }, [year, createDeathRate, updateDeathRate]);

    if (loading === true) {
        return (<CircularIndeterminate/>);
    } else {
        if (error !== null) {
            return (<h1>Error: {JSON.stringify(error)} ....</h1>);
        } else {
            if (data.length > 0) {
                return (
                    <MaterialTable
                        title=""
                        columns={newState.columns}
                        data={data}
                        editable={{
                            onRowAdd: (newData) =>
                                new Promise((resolve) => {
                                    setTimeout(() => {
                                        resolve();
                                        CreateDeathRate(year, newData.country, newData.femaleRate, newData.maleRate, state, dispatch)
                                    }, 600);
                                }),
                            onRowUpdate: (newData, oldData) =>
                                new Promise((resolve) => {
                                    setTimeout(() => {
                                        resolve();
                                        if (oldData) {
                                            console.log(oldData.toString())
                                            UpdateDeathRate(year, oldData.country, newData.femaleRate, newData.maleRate, state, dispatch)
                                        }
                                    }, 600);
                                }),
                        }}

                    />

                );
            } else {
                return (
                    <Fragment>
                        <h3>No data to display</h3>
                        <br/>
                        <FormCreateDeathRate/>
                    </Fragment>
                );
            }

        }
    }

}
