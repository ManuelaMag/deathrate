package com.example.deathrate.repositories;

import com.example.deathrate.datamodel.DeathIDData;
import com.example.deathrate.datamodel.DeathRateData;
import com.example.deathrate.datamodel.mappers.DeathRateDataMapper;
import com.example.deathrate.datatransferobjects.FindDeathRateDTO;
import com.example.deathrate.model.DeathRate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class DeathDBRepositoryTest {

    @Mock
    DeathJPARepository jpaRepository;

    @InjectMocks
    DeathDBRepository dbRepository;

    @Test
    @DisplayName("CreateDeathRate - Happy Case")
    void createDeathRate() {
        //Arrange
        DeathRate expected = new DeathRate(2002, "PT", 2.2, 3.1);
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData();
        deathIDData.setYear(2002);
        deathIDData.setCountry("PT");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.2);
        deathRateData.setMaleRate(3.1);

        Mockito.when(jpaRepository.save(deathRateData)).thenReturn(deathRateData);

        //Act
        DeathRate result = dbRepository.createDeathRate(2002, "PT", 2.2, 3.1);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("CreateDeathRate - Already Exists")
    void createDeathRateAlreadyExists() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData();
        deathIDData.setYear(2002);
        deathIDData.setCountry("PT");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.2);
        deathRateData.setMaleRate(3.1);

        Mockito.when(jpaRepository.existsById(deathIDData)).thenReturn(true);

        //Act
        Throwable result = assertThrows(IllegalStateException.class, () ->
                dbRepository.createDeathRate(2002, "PT", 2.2, 3.1));

        //Assert
        String expected = "This rate to year/country already exists";
        assertEquals(expected, result.getMessage());
    }

    @Test
    @DisplayName("UpdateDeathRate - Happy Case")
    void updateDeathRate() {
        //Arrange
        DeathRate expected = new DeathRate(2002, "PT", 2.4, 3.2);

        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData();
        deathIDData.setYear(2002);
        deathIDData.setCountry("PT");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.2);
        deathRateData.setMaleRate(3.1);

        DeathRateData savedData = new DeathRateData();
        savedData.setDeathID(deathIDData);
        savedData.setFemaleRate(2.4);
        savedData.setMaleRate(3.2);

        Mockito.when(jpaRepository.findById(deathIDData)).thenReturn(Optional.of(deathRateData));
        Mockito.when(jpaRepository.save(savedData)).thenReturn(savedData);

        //Act
        DeathRate result = dbRepository.updateDeathRate(2002, "PT", 2.4, 3.2);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("UpdateDeathRate - Exception - Rate isn't exist")
    void updateDeathRateNotExist() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData();
        deathIDData.setYear(2002);
        deathIDData.setCountry("PT");

        Mockito.when(jpaRepository.findById(deathIDData)).thenReturn(Optional.empty());


        //Act
        Throwable result = assertThrows(NullPointerException.class, () ->
                dbRepository.updateDeathRate(2002, "PT", 2.2, 3.1));

        //Assert
        String expected = "This rate don't exist";
        assertEquals(expected, result.getMessage());
    }

    @Test
    @DisplayName("Find All Year - Happy Case")
    void findAllYearDeathRate() {
        //Arrange
        DeathRate pt2017 = new DeathRate(2017, "PT", 2.4, 3.2);
        DeathRate uk2017 = new DeathRate(2017, "UK", 1.9, 4.2);
        DeathRate uk2016 = new DeathRate(2016, "UK", 1.7, 2.2);

        List<DeathRate> expected = new ArrayList<>();
        expected.add(pt2017);
        expected.add(uk2017);

        DeathRateData pt2017Data = DeathRateDataMapper.toData(pt2017);
        DeathRateData uk2017Data = DeathRateDataMapper.toData(uk2017);
        DeathRateData uk2016Data = DeathRateDataMapper.toData(uk2016);

        Iterable<DeathRateData> all = Arrays.asList(pt2017Data, uk2016Data, uk2017Data);
        Mockito.when(jpaRepository.findAll()).thenReturn(all);

        //Act
        List<DeathRate> result = dbRepository.findAllYearDeathRate(2017);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Find All Year - Empty")
    void findAllYearDeathRateEmpty() {
        //Arrange
        List<DeathRate> expected = new ArrayList<>();

        Iterable<DeathRateData> all = Arrays.asList();
        Mockito.when(jpaRepository.findAll()).thenReturn(all);

        //Act
        List<DeathRate> result = dbRepository.findAllYearDeathRate(2019);

        //Assert
        assertEquals(expected, result);
    }
}