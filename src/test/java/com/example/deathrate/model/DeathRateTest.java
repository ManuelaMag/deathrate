package com.example.deathrate.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class DeathRateTest {

    @Test
    @DisplayName("Get DeathRateID - Happy Case")
    void getDeathRateID() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        DeathRateID expected = new DeathRateID(2000, "PT");

        //Act
        DeathRateID deathRateID = deathRate.getDeathRateID();

        //Assert
        assertEquals(expected, deathRateID);
    }

    @Test
    @DisplayName("Get DeathRateID - NotEquals")
    void getDeathRateIDNotEquals() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        DeathRateID expected = new DeathRateID(2020, "PR");

        //Act
        DeathRateID deathRateID = deathRate.getDeathRateID();

        //Assert
        assertNotEquals(expected, deathRateID);
    }

    @Test
    @DisplayName("Get Female Rate - Happy Case")
    void getFemaleRate() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        Rate expected = new Rate(2.3);

        //Act
        Rate rate = deathRate.getFemaleRate();

        //Assert
        assertEquals(expected, rate);
    }

    @Test
    @DisplayName("Get Female Rate - NotEquals")
    void getFemaleRateNotEquals() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        Rate expected = new Rate(1.3);

        //Act
        Rate rate = deathRate.getFemaleRate();

        //Assert
        assertNotEquals(expected, rate);
    }

    @Test
    @DisplayName("Get male rate - Happy case")
    void getMaleRate() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        Rate expected = new Rate(1.15);

        //Act
        Rate rate = deathRate.getMaleRate();

        //Assert
        assertEquals(expected, rate);
    }

    @Test
    @DisplayName("Get male rate - NotEquals")
    void getMaleRateNotEquals() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        Rate expected = new Rate(2.15);

        //Act
        Rate rate = deathRate.getMaleRate();

        //Assert
        assertNotEquals(expected, rate);
    }

    @Test
    @DisplayName("Equals - Happy Case")
    void testEquals() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        DeathRate secondDeathRate = new DeathRate(2000, "PT", 2.3, 1.15);

        //Act+Assert
        assertEquals(deathRate, secondDeathRate);
    }

    @Test
    @DisplayName("Equals - Same object")
    void testEqualsSameObject() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);

        //Act+Assert
        assertEquals(deathRate, deathRate);
    }

    @Test
    @DisplayName("Equals - NotEquals")
    void testEqualsNotEquals() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        DeathRate secondDeathRate = new DeathRate(2019, "AU", 1.15, 2.3);

        //Act+Assert
        assertNotEquals(deathRate, secondDeathRate);
    }

    @Test
    @DisplayName("Equals - Null")
    void testEqualsNull() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);

        //Act+Assert
        assertNotEquals(deathRate, null);
    }

    @Test
    @DisplayName("Equals - Diff Class")
    void testEqualsDiffClass() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        DeathRateID deathRateID = new DeathRateID(2000, "PT");

        //Act+Assert
        assertNotEquals(deathRate, deathRateID);
    }

    @Test
    @DisplayName("HashCode - Happy Case")
    void testHashCode() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 1.15, 2.3);
        DeathRate secondDeathRate = new DeathRate(2000, "PT", 1.15, 2.3);

        //Act
        int hashDeath = deathRate.hashCode();
        int hashSecond = secondDeathRate.hashCode();
        //Assert
        assertEquals(hashDeath, hashSecond);
    }

    @Test
    @DisplayName("HashCode - Not Equals")
    void testHashCodeNotEquals() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);
        DeathRate secondDeathRate = new DeathRate(2019, "AU", 1.15, 2.3);

        //Act
        int hashDeath = deathRate.hashCode();
        int hashSecond = secondDeathRate.hashCode();
        //Assert
        assertNotEquals(hashDeath, hashSecond);
    }
}