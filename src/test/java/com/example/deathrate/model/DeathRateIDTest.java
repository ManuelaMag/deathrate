package com.example.deathrate.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class DeathRateIDTest {

    @Test
    @DisplayName("GetYear - happy case")
    void getYear() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");

        //Act
        int year = deathRateID.getYear();

        //Assert
        assertEquals(2000, year);
    }

    @Test
    @DisplayName("GetYear - NotEquals")
    void getYearNotEquals() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");

        //Act
        int year = deathRateID.getYear();

        //Assert
        assertNotEquals(0, year);
    }

    @Test
    @DisplayName("Get Country - Happy Case")
    void getCountry() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");

        //Act
        String country = deathRateID.getCountry();

        //Assert
        assertEquals("PT", country);
    }

    @Test
    @DisplayName("Get Country - NotEquals")
    void getCountryNotEquals() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");

        //Act
        String country = deathRateID.getCountry();

        //Assert
        assertNotEquals("UK", country);
    }

    @Test
    @DisplayName("Equals - Happy Case")
    void testEquals() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");
        DeathRateID secondDeathRateID = new DeathRateID(2000, "PT");

        //Act+Assert
        assertEquals(deathRateID, secondDeathRateID);
    }

    @Test
    @DisplayName("Equals - Same Object")
    void testEqualsSameObject() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");

        //Act+Assert
        assertEquals(deathRateID, deathRateID);
    }

    @Test
    @DisplayName("Equals - NotEquals")
    void testEqualsNotEquals() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");
        DeathRateID secondDeathRateID = new DeathRateID(2010, "UK");

        //Act+Assert
        assertNotEquals(deathRateID, secondDeathRateID);
    }

    @Test
    @DisplayName("Equals - Null")
    void testEqualsNull() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");

        //Act+Assert
        assertNotEquals(deathRateID, null);
    }

    @Test
    @DisplayName("Equals - Diff Class")
    void testEqualsDiffClass() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);

        //Act+Assert
        assertNotEquals(deathRateID, deathRate);
    }

    @Test
    @DisplayName("HashCode - Happy Case")
    void testHashCode() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");
        DeathRateID secondDeathRateID = new DeathRateID(2000, "PT");

        //Act
        int hashDeath = deathRateID.hashCode();
        int hashSecond = secondDeathRateID.hashCode();

        //Assert
        assertEquals(hashDeath, hashSecond);

    }

    @Test
    @DisplayName("HashCode - NotEquals")
    void testHashCodeNotEquals() {
        //Arrange
        DeathRateID deathRateID = new DeathRateID(2000, "PT");
        DeathRateID secondDeathRateID = new DeathRateID(2020, "AT");

        //Act
        int hashDeath = deathRateID.hashCode();
        int hashSecond = secondDeathRateID.hashCode();

        //Assert
        assertNotEquals(hashDeath, hashSecond);

    }
}