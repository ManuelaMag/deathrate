package com.example.deathrate.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RateTest {

    @Test
    @DisplayName("Get Rate - Happy Case")
    void getRate() {
        //Arrange
        Rate rate = new Rate(3.21);

        //Act
        double value = rate.getRate();

        //Assert
        assertEquals(3.21, value);
    }

    @Test
    @DisplayName("Get Rate - NotEquals")
    void getRateNotEquals() {
        //Arrange
        Rate rate = new Rate(3.21);

        //Act
        double value = rate.getRate();

        //Assert
        assertNotEquals(4.5, value);
    }

    @Test
    @DisplayName("Equals - Happy Case")
    void testEquals() {
        //Arrange
        Rate rate = new Rate(3.21);
        Rate secondRate = new Rate(3.21);

        //Act+Assert
        assertEquals(rate, secondRate);

    }

    @Test
    @DisplayName("Equals - Same object")
    void testEqualsSameObject() {
        //Arrange
        Rate rate = new Rate(3.21);

        //Act+Assert
        assertEquals(rate, rate);

    }

    @Test
    @DisplayName("Equals - Not Equals")
    void testEqualsNotEquals() {
        //Arrange
        Rate rate = new Rate(3.21);
        Rate secondRate = new Rate(4.5);

        //Act+Assert
        assertNotEquals(rate, secondRate);
    }

    @Test
    @DisplayName("Equals - Null")
    void testEqualsNull() {
        //Arrange
        Rate rate = new Rate(3.21);

        //Act+Assert
        assertNotEquals(rate, null);
    }

    @Test
    @DisplayName("Equals - Diff Class")
    void testEqualsDifClass() {
        //Arrange
        Rate rate = new Rate(3.21);
        DeathRateID deathRateID = new DeathRateID(2000, "PT");

        //Act+Assert
        assertNotEquals(rate, deathRateID);
    }

    @Test
    @DisplayName("HashCode - Happy Case")
    void testHashCode() {
        //Arrange
        Rate rate = new Rate(3.21);
        Rate secondRate = new Rate(3.21);

        //Act
        int hashRate = rate.hashCode();
        int hashSecond = secondRate.hashCode();

        //Assert
        assertEquals(hashRate, hashSecond);
    }

    @Test
    @DisplayName("HashCode - NotEquals")
    void testHashCodeNotEquals() {
        //Arrange
        Rate rate = new Rate(3.21);
        Rate secondRate = new Rate(4.25);

        //Act
        int hashRate = rate.hashCode();
        int hashSecond = secondRate.hashCode();

        //Assert
        assertNotEquals(hashRate, hashSecond);
    }
}