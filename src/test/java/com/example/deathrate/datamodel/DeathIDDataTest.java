package com.example.deathrate.datamodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeathIDDataTest {

    @Test
    @DisplayName("Get Year - Happy Case")
    void getYear() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act
        int year = deathIDData.getYear();

        //Assert
        assertEquals(2019, year);
    }

    @Test
    @DisplayName("Get Year - NotEquals")
    void getYearNotEquals() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act
        int year = deathIDData.getYear();

        //Assert
        assertNotEquals(2020, year);
    }

    @Test
    @DisplayName("Set Year - Happy Case")
    void setYear() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act
        deathIDData.setYear(2009);
        int year = deathIDData.getYear();
        //Assert
        assertEquals(2009, year);
    }

    @Test
    @DisplayName("Set Year - NotEquals")
    void setYearNotEquals() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act
        deathIDData.setYear(2009);
        int year = deathIDData.getYear();
        //Assert
        assertNotEquals(2019, year);
    }

    @Test
    @DisplayName("Get Country - Happy Case")
    void getCountry() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act
        String country = deathIDData.getCountry();

        //Assert
        assertEquals("UK", country);
    }

    @Test
    @DisplayName("Get Country - NotEquals")
    void getCountryNotEquals() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act
        String country = deathIDData.getCountry();

        //Assert
        assertNotEquals("PR", country);
    }


    @Test
    @DisplayName("Set Country - Happy Case")
    void setCountry() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act
        deathIDData.setCountry("PT");
        String country = deathIDData.getCountry();

        //Assert
        assertEquals("PT", country);
    }

    @Test
    @DisplayName("Set Country - NotEquals")
    void setCountryNotEquals() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act
        deathIDData.setCountry("PT");
        String country = deathIDData.getCountry();

        //Assert
        assertNotEquals("UK", country);
    }


    @Test
    @DisplayName("Equals - HappyCase")
    void testEquals() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");
        DeathIDData secondDeathIDData = new DeathIDData(2019, "UK");

        //Act+Assert
        assertEquals(deathIDData, secondDeathIDData);
    }

    @Test
    @DisplayName("Equals - Same object")
    void testEqualsSameObject() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act+Assert
        assertEquals(deathIDData, deathIDData);
    }

    @Test
    @DisplayName("Equals - Not Equals")
    void testEqualsDiffObject() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");
        DeathIDData secondDeathIDData = new DeathIDData(2009, "PT");

        //Act+Assert
        assertNotEquals(deathIDData, secondDeathIDData);
    }

    @Test
    @DisplayName("Equals - Null")
    void testEqualsNull() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");

        //Act+Assert
        assertNotEquals(deathIDData, null);
    }

    @Test
    @DisplayName("Equals - Diff class")
    void testEqualsDiffClass() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");
        DeathRateData deathRateData = new DeathRateData();

        //Act+Assert
        assertNotEquals(deathIDData, deathRateData);
    }

    @Test
    @DisplayName("Hascode - Happy Case")
    void testHashCode() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");
        DeathIDData secondDeathIDData = new DeathIDData(2019, "UK");

        //Act
        int hashDeath = deathIDData.hashCode();
        int hashSecond = secondDeathIDData.hashCode();

        //Assert
        assertEquals(hashDeath, hashSecond);
    }

    @Test
    @DisplayName("Hascode - NotEquals")
    void testHashCodeNotEquals() {
        //Arrange
        DeathIDData deathIDData = new DeathIDData(2019, "UK");
        DeathIDData secondDeathIDData = new DeathIDData(2009, "PR");

        //Act
        int hashDeath = deathIDData.hashCode();
        int hashSecond = secondDeathIDData.hashCode();

        //Assert
        assertNotEquals(hashDeath, hashSecond);
    }
}