package com.example.deathrate.datamodel;

import com.example.deathrate.model.Rate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeathRateDataTest {

    @Test
    @DisplayName("Set and Get DeathID - Happy Case")
    void setDeathID() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        DeathIDData newDeathIDData = new DeathIDData(2010, "PT");

        //Act
        deathRateData.setDeathID(newDeathIDData);
        DeathIDData result = deathRateData.getDeathID();

        //Assert
        assertEquals(newDeathIDData, result);
    }

    @Test
    @DisplayName("Set and Get DeathID - NotEquals")
    void setDeathIDNotEquals() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        DeathIDData newDeathIDData = new DeathIDData(2010, "PT");

        //Act
        deathRateData.setDeathID(newDeathIDData);
        DeathIDData result = deathRateData.getDeathID();

        //Assert
        assertNotEquals(deathIDData, result);
    }

    @Test
    @DisplayName("Set and Get FemaleRate - Happy Case")
    void setFemaleRate() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        //Act
        deathRateData.setFemaleRate(2.04);
        double result = deathRateData.getFemaleRate();

        //Assert
        assertEquals(2.04, result);
    }

    @Test
    @DisplayName("Set and Get FemaleRate - NotEquals")
    void setFemaleRateNotEquals() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        //Act
        deathRateData.setFemaleRate(2.04);
        double result = deathRateData.getFemaleRate();

        //Assert
        assertNotEquals(2.34, result);
    }

    @Test
    @DisplayName("Set and Get MaleRate - Happy Case")
    void setMaleRate() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        //Act
        deathRateData.setMaleRate(2.44);
        double result = deathRateData.getMaleRate();

        //Assert
        assertEquals(2.44, result);
    }

    @Test
    @DisplayName("Set and Get MaleRate - Not Equals")
    void setMaleRateNotEquals() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        //Act
        deathRateData.setMaleRate(2.44);
        double result = deathRateData.getMaleRate();

        //Assert
        assertNotEquals(3.44, result);
    }

    @Test
    @DisplayName("Equals - Happy Case")
    void testEquals() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        DeathRateData secondDeathRateData = new DeathRateData();
        secondDeathRateData.setDeathID(deathIDData);
        secondDeathRateData.setFemaleRate(2.34);
        secondDeathRateData.setMaleRate(3.44);

        //Act+Assert
        assertEquals(deathRateData, secondDeathRateData);
    }

    @Test
    @DisplayName("Equals - Same Object")
    void testEqualsSameObject() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);
        //Act+Assert
        assertEquals(deathRateData, deathRateData);
    }

    @Test
    @DisplayName("Equals - NotEquals")
    void testEqualsNotEquals() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        DeathRateData secondDeathRateData = new DeathRateData();
        DeathIDData secondDeathIDData = new DeathIDData(2001, "uk");
        secondDeathRateData.setDeathID(secondDeathIDData);
        secondDeathRateData.setFemaleRate(2.00);
        secondDeathRateData.setMaleRate(1.9);

        //Act+Assert
        assertNotEquals(deathRateData, secondDeathRateData);
    }

    @Test
    @DisplayName("Equals - Null")
    void testEqualsNull() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        //Act+Assert
        assertNotEquals(deathRateData, null);
    }

    @Test
    @DisplayName("Equals - Diff Class")
    void testEqualsDiffClass() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        //Act+Assert
        assertNotEquals(deathRateData, deathIDData);
    }

    @Test
    @DisplayName("HashCode - HappyCase")
    void testHashCode() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        DeathRateData secondDeathRateData = new DeathRateData();
        secondDeathRateData.setDeathID(deathIDData);
        secondDeathRateData.setFemaleRate(2.34);
        secondDeathRateData.setMaleRate(3.44);

        //Act
        int hashDeathRate = deathRateData.hashCode();
        int hashSecond = secondDeathRateData.hashCode();

        //Assert
        assertEquals(hashDeathRate, hashSecond);
    }

    @Test
    @DisplayName("HashCode - NotEquals")
    void testHashCodeNotEquals() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2011, "PR");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.34);
        deathRateData.setMaleRate(3.44);

        DeathRateData secondDeathRateData = new DeathRateData();
        DeathIDData secondDeathIDData = new DeathIDData(2001, "uk");
        secondDeathRateData.setDeathID(secondDeathIDData);
        secondDeathRateData.setFemaleRate(2.00);
        secondDeathRateData.setMaleRate(1.9);

        //Act
        int hashDeathRate = deathRateData.hashCode();
        int hashSecond = secondDeathRateData.hashCode();

        //Assert
        assertNotEquals(hashDeathRate, hashSecond);

    }
}