package com.example.deathrate.datamodel.mappers;

import com.example.deathrate.datamodel.DeathIDData;
import com.example.deathrate.datamodel.DeathRateData;
import com.example.deathrate.model.DeathRate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeathRateDataMapperTest {

    @Test
    @DisplayName("DeathRateData - ToData")
    void toData() {
        //Arrange
        DeathRate deathRate = new DeathRate(2003, "uk", 2.02, 3.4);
        DeathRateData expected = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2003, "UK");
        expected.setDeathID(deathIDData);
        expected.setFemaleRate(2.02);
        expected.setMaleRate(3.4);

        //Act
        DeathRateData deathRateData = DeathRateDataMapper.toData(deathRate);

        //Assert
        assertEquals(expected, deathRateData);
    }

    @Test
    @DisplayName("DeathRateData - ToDomain")
    void toDomain() {
        //Arrange
        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathIDData = new DeathIDData(2003, "UK");
        deathRateData.setDeathID(deathIDData);
        deathRateData.setFemaleRate(2.02);
        deathRateData.setMaleRate(3.4);

        DeathRate expected = new DeathRate(2003, "UK", 2.02, 3.4);

        //Act
        DeathRate deathRate = DeathRateDataMapper.toDomain(deathRateData);

        //Assert
        assertEquals(expected, deathRate);

    }
}