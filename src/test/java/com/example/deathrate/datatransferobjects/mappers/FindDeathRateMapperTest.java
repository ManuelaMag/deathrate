package com.example.deathrate.datatransferobjects.mappers;

import com.example.deathrate.datatransferobjects.DeathRateDTO;
import com.example.deathrate.datatransferobjects.FindDeathRateDTO;
import com.example.deathrate.model.DeathRate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FindDeathRateMapperTest {

    @Test
    @DisplayName("Find Death Rate Mapper - Happy Case")
    void mapper() {
        //Arrange
        List<DeathRate> list = new ArrayList<>();
        DeathRate pt2017 = new DeathRate(2017, "PT", 2.4, 3.2);
        DeathRate uk2017 = new DeathRate(2017, "UK", 1.9, 4.2);
        list.add(pt2017);
        list.add(uk2017);

        FindDeathRateDTO expected = new FindDeathRateDTO();
        List<DeathRateDTO> listDTO = new ArrayList<>();
        listDTO.add(DeathRateMapper.mapper(pt2017));
        listDTO.add(DeathRateMapper.mapper(uk2017));
        expected.setDeathRateList(listDTO);

        String linkExpected = "deathrates/2017";

        //Act
        FindDeathRateDTO result = FindDeathRateMapper.mapper(list);

        String linkResult = result.getLink("self").get().toString();

        //Assert
        assertAll(
                () -> assertEquals(expected, result),
                () -> assertTrue(linkResult.contains(linkExpected))
        );
    }
}