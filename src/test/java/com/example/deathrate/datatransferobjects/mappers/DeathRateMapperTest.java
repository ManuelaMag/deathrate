package com.example.deathrate.datatransferobjects.mappers;

import com.example.deathrate.datatransferobjects.DeathRateDTO;
import com.example.deathrate.model.DeathRate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeathRateMapperTest {

    @Test
    @DisplayName("Death Rate Mapper - Happy Case")
    void mapper() {
        //Arrange
        DeathRate deathRate = new DeathRate(2000, "PT", 2.3, 1.15);

        DeathRateDTO expected = new DeathRateDTO();
        expected.setYear(2000);
        expected.setCountry("PT");
        expected.setFemaleRate(2.3);
        expected.setMaleRate(1.15);

        String linkExpected = "deathrates/2000/countries/PT";

        //Act
        DeathRateDTO result = DeathRateMapper.mapper(deathRate);

        String link = result.getLink("self").get().toString();
        //Assert
        assertAll(
                () -> assertEquals(expected, result),
                () -> assertTrue(link.contains(linkExpected))
        );
    }
}