package com.example.deathrate.services;

import com.example.deathrate.datatransferobjects.CreateDeathRateDTO;
import com.example.deathrate.datatransferobjects.DeathRateDTO;
import com.example.deathrate.model.DeathRate;
import com.example.deathrate.repositories.DeathRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
@ExtendWith(MockitoExtension.class)
class CreateDeathRateServiceTest {
    @InjectMocks
    CreateDeathRateService service;

    @Mock
    DeathRepository deathRepository;

    @Test
    @DisplayName("Create Death Rate - Happy Case")
    void createDeathRate() {
        //Arrange
        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setYear(2019);
        inDTO.setCountry("it");
        inDTO.setFemaleRate(2.0);
        inDTO.setMaleRate(3.4);

        DeathRate deathRate = new DeathRate(2019, "IT", 2.0, 3.4);

        Mockito.when(deathRepository.createDeathRate(2019, "it", 2.0, 3.4)).thenReturn(deathRate);

        DeathRateDTO expected = new DeathRateDTO();
        expected.setYear(2019);
        expected.setCountry("IT");
        expected.setFemaleRate(2.0);
        expected.setMaleRate(3.4);

        //Act
        DeathRateDTO result = service.createDeathRate(inDTO);

        //Assert
        assertEquals(expected, result);
    }
}