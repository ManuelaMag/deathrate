package com.example.deathrate.services;

import com.example.deathrate.datatransferobjects.CreateDeathRateDTO;
import com.example.deathrate.datatransferobjects.DeathRateDTO;
import com.example.deathrate.datatransferobjects.FindDeathRateDTO;
import com.example.deathrate.datatransferobjects.mappers.FindDeathRateMapper;
import com.example.deathrate.model.DeathRate;
import com.example.deathrate.repositories.DeathRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
@ExtendWith(MockitoExtension.class)
class FindDeathRateServiceTest {

    @InjectMocks
    FindDeathRateService service;

    @Mock
    DeathRepository deathRepository;


    @Test
    @DisplayName("Find all death rate per year ")
    void findAllYearDeathRate() {
        //Arrange
        int year = 2019;

        DeathRate deathRateIT = new DeathRate(2019, "IT", 2.0, 3.4);
        DeathRate deathRatePT = new DeathRate(2019, "PT", 2.3, 2.9);
        List<DeathRate> listDB = new ArrayList<>();
        listDB.add(deathRateIT);
        listDB.add(deathRatePT);

        Mockito.when(deathRepository.findAllYearDeathRate(year)).thenReturn(listDB);

        FindDeathRateDTO expected = FindDeathRateMapper.mapper(listDB);

        //Act
        FindDeathRateDTO result = service.findAllYearDeathRate(year);

        //Assert
        assertEquals(expected, result);
    }
}