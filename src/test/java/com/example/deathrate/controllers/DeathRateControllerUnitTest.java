package com.example.deathrate.controllers;

import com.example.deathrate.datatransferobjects.CreateDeathRateDTO;
import com.example.deathrate.datatransferobjects.DeathRateDTO;
import com.example.deathrate.datatransferobjects.FindDeathRateDTO;
import com.example.deathrate.datatransferobjects.mappers.DeathRateMapper;
import com.example.deathrate.datatransferobjects.mappers.FindDeathRateMapper;
import com.example.deathrate.model.DeathRate;
import com.example.deathrate.services.CreateDeathRateService;
import com.example.deathrate.services.FindDeathRateService;
import com.example.deathrate.services.UpdateDeathRateService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
@ExtendWith(MockitoExtension.class)
public class DeathRateControllerUnitTest {

    @InjectMocks
    DeathRateController controller;

    @Mock
    CreateDeathRateService createService;

    @Mock
    UpdateDeathRateService updateService;

    @Mock
    FindDeathRateService findService;

    @Test
    @DisplayName("Create Death Rate - Happy Case")
    void createDeathRate() {
        //Arrange
        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setCountry("it");
        inDTO.setFemaleRate(2.3);
        inDTO.setMaleRate(4.4);

        DeathRate deathRate = new DeathRate(2010, "it", 2.3, 4.4);

        DeathRateDTO deathRateDTO = DeathRateMapper.mapper(deathRate);

        Mockito.when(createService.createDeathRate(inDTO)).thenReturn(deathRateDTO);
        //Act
        ResponseEntity<Object> result = controller.createDeathRate(2010, inDTO);


        //Assert
        assertAll(
                () -> assertEquals(HttpStatus.CREATED.value(), result.getStatusCodeValue()),
                () -> assertTrue(Objects.requireNonNull(result.getBody().toString().contains("deathrates/2010/countries/IT>;rel=\"self\"")))
        );

    }

    @Test
    @DisplayName("Update Death Rate - Happy Case")
    void updateDeathRate() {
        //Arrange
        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setFemaleRate(2.3);
        inDTO.setMaleRate(4.4);

        DeathRate deathRate = new DeathRate(2010, "it", 2.3, 4.4);

        DeathRateDTO deathRateDTO = DeathRateMapper.mapper(deathRate);

        Mockito.when(updateService.updateDeathRate(inDTO)).thenReturn(deathRateDTO);
        //Act
        ResponseEntity<Object> result = controller.updateDeathRate(2010, "IT", inDTO);


        //Assert
        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), result.getStatusCodeValue()),
                () -> assertTrue(Objects.requireNonNull(result.getBody().toString().contains("deathrates/2010/countries/IT>;rel=\"self\"")))
        );

    }

    @Test
    @DisplayName("Find All Death Rate per year - Happy Case")
    void findAllYearDeathRate() {
        //Arrange
        int year = 2008;

        DeathRate deathRateIT = new DeathRate(2008, "IT", 2.0, 3.4);
        DeathRate deathRatePT = new DeathRate(2008, "PT", 2.3, 2.9);
        List<DeathRate> listDB = new ArrayList<>();
        listDB.add(deathRateIT);
        listDB.add(deathRatePT);
        FindDeathRateDTO findDeathRateDTO = FindDeathRateMapper.mapper(listDB);

        Mockito.when(findService.findAllYearDeathRate(year)).thenReturn(findDeathRateDTO);
        //Act
        ResponseEntity<Object> result = controller.findAllYearDeathRate(2008);

        //Assert
        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), result.getStatusCodeValue()),
                () -> assertTrue(Objects.requireNonNull(result.getBody().toString().contains("deathrates/2008>;rel=\"self\"")))
        );

    }

}
