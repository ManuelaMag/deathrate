package com.example.deathrate.controllers;

import com.example.deathrate.datatransferobjects.CreateDeathRateDTO;
import com.example.deathrate.repositories.DeathRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@WebAppConfiguration
class DeathRateControllerIntTest {

    @Autowired
    WebApplicationContext webApplicationContext;
    @Autowired
    DeathRepository deathRepository;
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;

    @BeforeAll
    private void setUp() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @BeforeEach
    private void clean() {
        deathRepository.deleteAll();
    }

    @Test
    @DisplayName("Create Death Rate - Happy Case")
    void createDeathRate() throws Exception {
        //Arrange
        final String postUri = "/deathrates/2009/countries";

        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setCountry("it");
        inDTO.setFemaleRate(2.044);
        inDTO.setMaleRate(3.4);
        String inputJson = objectMapper.writeValueAsString(inDTO);

        //Act + Assert
        mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.*", hasSize(5)))
                .andExpect(jsonPath("$.year").value(2009))
                .andExpect(jsonPath("$.country").value("IT"))
                .andExpect(jsonPath("$.femaleRate").value(2.04))
                .andExpect(jsonPath("$.maleRate").value(3.4))
                .andExpect(jsonPath("$.links.[0].href", containsString("/deathrates/2009/countries/IT")));
    }

    @Test
    @DisplayName("Create Death Rate - Already Exists")
    void createDeathRateAlreadyExists() throws Exception {
        //Arrange
        final String postUri = "/deathrates/2009/countries";

        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setCountry("it");
        inDTO.setFemaleRate(2.0);
        inDTO.setMaleRate(3.4);
        String inputJson = objectMapper.writeValueAsString(inDTO);

        deathRepository.createDeathRate(2009, "it", 2.0, 3.4);
        //Act + Assert
        mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.details").value("This rate to year/country already exists"));
    }

    @Test
    @DisplayName("Create Death Rate - Country don't Exist")
    void createDeathRateCountryNotExists() throws Exception {
        //Arrange
        final String postUri = "/deathrates/2009/countries";

        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setCountry("portugal");
        inDTO.setFemaleRate(2.0);
        inDTO.setMaleRate(3.4);
        String inputJson = objectMapper.writeValueAsString(inDTO);

        //Act + Assert
        mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.details").value("This country code don't exist"));
    }

    @Test
    @DisplayName("Create Death Rate - Year Error")
    void createDeathRateYearError() throws Exception {
        //Arrange
        final String postUri = "/deathrates/2029/countries";

        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setCountry("portugal");
        inDTO.setFemaleRate(2.0);
        inDTO.setMaleRate(3.4);
        String inputJson = objectMapper.writeValueAsString(inDTO);

        //Act + Assert
        mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.details").value("This year isn't possible to register"));
    }

    @Test
    @DisplayName("Create Death Rate - Rate between 0.00 and 1000.00")
    void createDeathRateRateError() throws Exception {
        //Arrange
        final String postUri = "/deathrates/2009/countries";

        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setCountry("it");
        inDTO.setFemaleRate(2.0);
        inDTO.setMaleRate(1001.00);
        String inputJson = objectMapper.writeValueAsString(inDTO);

        //Act + Assert
        mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.details").value("Insert the value between 0.00 and 1000.00"));
    }

    @Test
    @DisplayName("Update Death Rate - Happy Case")
    void updateDeathRate() throws Exception {
        //Arrange
        final String postUri = "/deathrates/2009/countries/IT";

        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setFemaleRate(2.044);
        inDTO.setMaleRate(3.4);
        String inputJson = objectMapper.writeValueAsString(inDTO);

        deathRepository.createDeathRate(2009, "it", 2.3, 3.2);

        //Act + Assert
        mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(5)))
                .andExpect(jsonPath("$.year").value(2009))
                .andExpect(jsonPath("$.country").value("IT"))
                .andExpect(jsonPath("$.femaleRate").value(2.04))
                .andExpect(jsonPath("$.maleRate").value(3.4))
                .andExpect(jsonPath("$.links.[0].href", containsString("/deathrates/2009/countries/IT")));
    }

    @Test
    @DisplayName("Update Death Rate - Doesn´t Exist")
    void updateDeathRateNotExists() throws Exception {
        //Arrange
        final String postUri = "/deathrates/2009/countries/IT";

        CreateDeathRateDTO inDTO = new CreateDeathRateDTO();
        inDTO.setCountry("it");
        inDTO.setFemaleRate(2.0);
        inDTO.setMaleRate(3.4);
        String inputJson = objectMapper.writeValueAsString(inDTO);

        //Act + Assert
        mvc.perform(MockMvcRequestBuilders.post(postUri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.details").value("This rate don't exist"));
    }

    @Test
    @DisplayName("Find All Death Rate per year - Happy Case")
    void findAllYearDeathRate() throws Exception {
        //Arrange
        final String getURI = "/deathrates/2009";

        deathRepository.createDeathRate(2009, "it", 2.3, 3.2);
        deathRepository.createDeathRate(2009, "PT", 1.3, 2.2);

        //Act + Assert
        mvc.perform(MockMvcRequestBuilders.get(getURI)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.deathRateList", hasSize(2)))
                .andExpect(jsonPath("$.deathRateList[0].links.[0].href", containsString("/deathrates/2009/countries/IT")))
                .andExpect(jsonPath("$.deathRateList[1].links.[0].href", containsString("/deathrates/2009/countries/PT")))
                .andExpect(jsonPath("$.links.[0].href", containsString("/deathrates/2009")));
    }
    @Test
    @DisplayName("Find All Death Rate per year - Empty list")
    void findAllYearDeathRateEmptyList() throws Exception {
        //Arrange
        final String getURI = "/deathrates/2019";

        deathRepository.createDeathRate(2009, "it", 2.3, 3.2);
        deathRepository.createDeathRate(2009, "PT", 1.3, 2.2);

        //Act + Assert
        mvc.perform(MockMvcRequestBuilders.get(getURI)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.deathRateList").isEmpty());
    }
}