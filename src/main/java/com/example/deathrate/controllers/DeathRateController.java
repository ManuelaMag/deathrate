package com.example.deathrate.controllers;

import com.example.deathrate.datatransferobjects.CreateDeathRateDTO;
import com.example.deathrate.services.CreateDeathRateService;
import com.example.deathrate.services.FindDeathRateService;
import com.example.deathrate.services.UpdateDeathRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("deathrates")
public class DeathRateController {

    private CreateDeathRateService createService;
    private UpdateDeathRateService updateService;
    private FindDeathRateService findService;

    @Autowired
    public DeathRateController(CreateDeathRateService createService, UpdateDeathRateService updateService, FindDeathRateService findService) {
        this.createService = createService;
        this.updateService = updateService;
        this.findService = findService;
    }

    @PostMapping("/{year}/countries")
    public ResponseEntity<Object> createDeathRate(@PathVariable int year, @RequestBody CreateDeathRateDTO inDTO) {
        inDTO.setYear(year);

        return new ResponseEntity<>(createService.createDeathRate(inDTO), HttpStatus.CREATED);
    }

    @PostMapping("/{year}/countries/{country}")
    public ResponseEntity<Object> updateDeathRate(@PathVariable int year, @PathVariable String country, @RequestBody CreateDeathRateDTO inDTO) {
        inDTO.setYear(year);
        inDTO.setCountry(country);

        return new ResponseEntity<>(updateService.updateDeathRate(inDTO), HttpStatus.OK);
    }

    @GetMapping("/{year}")
    public ResponseEntity<Object> findAllYearDeathRate(@PathVariable int year) {
        return new ResponseEntity<>(findService.findAllYearDeathRate(year), HttpStatus.OK);
    }

}
