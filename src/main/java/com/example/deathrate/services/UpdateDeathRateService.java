package com.example.deathrate.services;

import com.example.deathrate.datatransferobjects.CreateDeathRateDTO;
import com.example.deathrate.datatransferobjects.DeathRateDTO;
import com.example.deathrate.datatransferobjects.mappers.DeathRateMapper;
import com.example.deathrate.model.DeathRate;
import com.example.deathrate.repositories.DeathRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateDeathRateService {

    DeathRepository deathRepository;

    @Autowired
    public UpdateDeathRateService(DeathRepository deathRepository) {
        this.deathRepository = deathRepository;
    }

    public DeathRateDTO updateDeathRate(CreateDeathRateDTO inDTO) {
        int year = inDTO.getYear();
        String country = inDTO.getCountry();
        double femaleRate = inDTO.getFemaleRate();
        double maleRate = inDTO.getMaleRate();
        DeathRate newDeathRate = deathRepository.updateDeathRate(year, country, femaleRate, maleRate);

        return DeathRateMapper.mapper(newDeathRate);
    }
}
