package com.example.deathrate.services;

import com.example.deathrate.datatransferobjects.FindDeathRateDTO;
import com.example.deathrate.datatransferobjects.mappers.FindDeathRateMapper;
import com.example.deathrate.model.DeathRate;
import com.example.deathrate.repositories.DeathRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FindDeathRateService {

    DeathRepository deathRepository;

    @Autowired
    public FindDeathRateService(DeathRepository deathRepository) {
        this.deathRepository = deathRepository;
    }

    public FindDeathRateDTO findAllYearDeathRate(int year) {
        List<DeathRate> list = deathRepository.findAllYearDeathRate(year);

        return FindDeathRateMapper.mapper(list);
    }
}
