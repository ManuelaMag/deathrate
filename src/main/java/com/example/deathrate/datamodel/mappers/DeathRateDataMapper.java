package com.example.deathrate.datamodel.mappers;

import com.example.deathrate.datamodel.DeathIDData;
import com.example.deathrate.datamodel.DeathRateData;
import com.example.deathrate.model.DeathRate;

public class DeathRateDataMapper {

    public static DeathRateData toData(DeathRate deathRate) {

        DeathRateData deathRateData = new DeathRateData();
        DeathIDData deathID = new DeathIDData(deathRate.getDeathRateID().getYear(), deathRate.getDeathRateID().getCountry());
        deathRateData.setDeathID(deathID);
        deathRateData.setFemaleRate(deathRate.getFemaleRate().getRate());
        deathRateData.setMaleRate(deathRate.getMaleRate().getRate());

        return deathRateData;
    }

    public static DeathRate toDomain(DeathRateData deathRateData) {

        int year = deathRateData.getDeathID().getYear();
        String country = deathRateData.getDeathID().getCountry();
        double femaleRate = deathRateData.getFemaleRate();
        double maleRate = deathRateData.getMaleRate();

        return new DeathRate(year, country, femaleRate, maleRate);
    }
}
