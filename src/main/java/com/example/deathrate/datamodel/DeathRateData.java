package com.example.deathrate.datamodel;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class DeathRateData {

    @Id
    private DeathIDData deathID;
    private double femaleRate;
    private double maleRate;

    public DeathIDData getDeathID() {
        return deathID;
    }

    public void setDeathID(DeathIDData deathID) {
        this.deathID = deathID;
    }

    public double getFemaleRate() {
        return femaleRate;
    }

    public void setFemaleRate(double femaleRate) {
        this.femaleRate = femaleRate;
    }

    public double getMaleRate() {
        return maleRate;
    }

    public void setMaleRate(double maleRate) {
        this.maleRate = maleRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeathRateData that = (DeathRateData) o;
        return Double.compare(that.femaleRate, femaleRate) == 0 &&
                Double.compare(that.maleRate, maleRate) == 0 &&
                Objects.equals(deathID, that.deathID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deathID, femaleRate, maleRate);
    }
}
