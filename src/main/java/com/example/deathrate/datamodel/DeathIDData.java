package com.example.deathrate.datamodel;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DeathIDData implements Serializable {

    private int year;
    private String country;

    public DeathIDData() {
    }

    public DeathIDData(int year, String country) {
        this.year = year;
        this.country = country;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeathIDData that = (DeathIDData) o;
        return year == that.year &&
                Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, country);
    }
}
