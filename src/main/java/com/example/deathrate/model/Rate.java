package com.example.deathrate.model;

import java.text.DecimalFormat;
import java.util.Objects;

public class Rate {
    private double rate;

    public Rate(double rate) {
        if (!checkRate(rate)) throw new IllegalArgumentException("Insert the value between 0.00 and 1000.00");
        this.rate = Math.round(rate * 100) / 100d;
    }

    private boolean checkRate(double rate) {
        if (0.00 <= rate && rate <= 1000.00) return true;
        return false;
    }

    public double getRate() {
        return rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rate rate1 = (Rate) o;
        return Double.compare(rate1.rate, rate) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rate);
    }
}
