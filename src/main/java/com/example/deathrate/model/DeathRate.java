package com.example.deathrate.model;

import java.util.Objects;

public class DeathRate {
    private DeathRateID deathRateID;
    private Rate femaleRate;
    private Rate maleRate;

    public DeathRate(int year, String country, double femaleRate, double maleRate) {
        this.deathRateID = new DeathRateID(year, country);
        this.femaleRate = new Rate(femaleRate);
        this.maleRate = new Rate(maleRate);
    }

    public DeathRateID getDeathRateID() {
        return deathRateID;
    }

    public Rate getFemaleRate() {
        return femaleRate;
    }

    public Rate getMaleRate() {
        return maleRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeathRate deathRate = (DeathRate) o;
        return Objects.equals(deathRateID, deathRate.deathRateID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deathRateID);
    }
}
