package com.example.deathrate.model;

import com.neovisionaries.i18n.CountryCode;

import java.util.Calendar;
import java.util.Objects;

public class DeathRateID {
    private int year;
    private String country;

    public DeathRateID(int year, String country) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        if (year > currentYear) throw new IllegalArgumentException("This year isn't possible to register");
        this.year = year;

        if (!verifyCountryCode(country)) throw new IllegalArgumentException("This country code don't exist");
        this.country = country.toUpperCase();
    }

    private boolean verifyCountryCode(String code) {
        CountryCode[] codeList = CountryCode.values();
        for (CountryCode unitCode : codeList) {
            if (unitCode.getAlpha2().equals(code.toUpperCase())) return true;
        }
        return false;
    }

    public int getYear() {
        return year;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeathRateID deathRateID = (DeathRateID) o;
        return year == deathRateID.year &&
                this.country.equals(deathRateID.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, country);
    }
}
