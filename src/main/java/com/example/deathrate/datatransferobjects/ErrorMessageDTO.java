package com.example.deathrate.datatransferobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ErrorMessageDTO {
    private String message;
    private List<String> details;

    public ErrorMessageDTO(String message) {
        this.message = message;
    }

    public ErrorMessageDTO(String message, List<String> details) {
        this.message = message;
        this.details = new ArrayList<>(details);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getDetails() {
        return details != null ? new ArrayList<>(details) : null;
    }

    public void setDetails(List<String> details) {
        this.details = new ArrayList<>(details);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ErrorMessageDTO that = (ErrorMessageDTO) o;
        return message.equals(that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }

    @Override
    public String toString() {
        return "ErrorMessageDTO{" +
                "message='" + message + '\'' +
                ", details=" + details +
                '}';
    }

}
