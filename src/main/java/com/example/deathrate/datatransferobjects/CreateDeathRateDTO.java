package com.example.deathrate.datatransferobjects;


import java.util.Objects;

public class CreateDeathRateDTO {

    private int year;
    private String country;
    private double femaleRate;
    private double maleRate;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getFemaleRate() {
        return femaleRate;
    }

    public void setFemaleRate(double femaleRate) {
        this.femaleRate = femaleRate;
    }

    public double getMaleRate() {
        return maleRate;
    }

    public void setMaleRate(double maleRate) {
        this.maleRate = maleRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateDeathRateDTO that = (CreateDeathRateDTO) o;
        return year == that.year &&
                Objects.equals(country, that.country) &&
                Objects.equals(femaleRate, that.femaleRate) &&
                Objects.equals(maleRate, that.maleRate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, country, femaleRate, maleRate);
    }
}
