package com.example.deathrate.datatransferobjects.mappers;

import com.example.deathrate.controllers.DeathRateController;
import com.example.deathrate.datatransferobjects.DeathRateDTO;
import com.example.deathrate.datatransferobjects.FindDeathRateDTO;
import com.example.deathrate.model.DeathRate;
import org.springframework.hateoas.Link;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class FindDeathRateMapper {

    public static FindDeathRateDTO mapper(List<DeathRate> list) {

        List<DeathRateDTO> deathRateDTOList = new ArrayList<>();
        for (DeathRate deathRate : list) {
            deathRateDTOList.add(DeathRateMapper.mapper(deathRate));
        }

        FindDeathRateDTO dto = new FindDeathRateDTO();
        dto.setDeathRateList(deathRateDTOList);
        if (!deathRateDTOList.isEmpty()) {
            Link self;
            int year = deathRateDTOList.get(0).getYear();
            self = linkTo(DeathRateController.class).slash(year).withSelfRel();
            dto.add(self);
        }
        return dto;

    }
}
