package com.example.deathrate.datatransferobjects.mappers;

import com.example.deathrate.controllers.DeathRateController;
import com.example.deathrate.datatransferobjects.DeathRateDTO;
import com.example.deathrate.model.DeathRate;
import org.springframework.hateoas.Link;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class DeathRateMapper {

    public static DeathRateDTO mapper(DeathRate deathRate) {
        DeathRateDTO dto = new DeathRateDTO();
        int year = deathRate.getDeathRateID().getYear();
        dto.setYear(year);
        String country = deathRate.getDeathRateID().getCountry();
        dto.setCountry(country);
        dto.setFemaleRate(deathRate.getFemaleRate().getRate());
        dto.setMaleRate(deathRate.getMaleRate().getRate());

        Link self;

        self = linkTo(DeathRateController.class).slash(year).slash("countries").slash(country).withSelfRel();
        dto.add(self);

        return dto;

    }

}
