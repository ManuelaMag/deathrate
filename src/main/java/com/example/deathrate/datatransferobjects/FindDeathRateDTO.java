package com.example.deathrate.datatransferobjects;


import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class FindDeathRateDTO extends RepresentationModel<FindDeathRateDTO> {
    private List<DeathRateDTO> deathRateList;

    public List<DeathRateDTO> getDeathRateList() {
        return deathRateList;
    }

    public void setDeathRateList(List<DeathRateDTO> deathRateList) {
        this.deathRateList = deathRateList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FindDeathRateDTO that = (FindDeathRateDTO) o;
        return Objects.equals(deathRateList, that.deathRateList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deathRateList);
    }
}
