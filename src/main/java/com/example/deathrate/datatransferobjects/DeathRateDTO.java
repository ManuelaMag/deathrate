package com.example.deathrate.datatransferobjects;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class DeathRateDTO extends RepresentationModel<DeathRateDTO> {

    private int year;
    private String country;
    private double femaleRate;
    private double maleRate;

    public DeathRateDTO() {
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getFemaleRate() {
        return femaleRate;
    }

    public void setFemaleRate(double femaleRate) {
        this.femaleRate = femaleRate;
    }

    public double getMaleRate() {
        return maleRate;
    }

    public void setMaleRate(double maleRate) {
        this.maleRate = maleRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeathRateDTO that = (DeathRateDTO) o;
        return year == that.year &&
                Double.compare(that.femaleRate, femaleRate) == 0 &&
                Double.compare(that.maleRate, maleRate) == 0 &&
                Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, country, femaleRate, maleRate);
    }
}
