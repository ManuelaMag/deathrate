package com.example.deathrate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeathrateApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeathrateApplication.class, args);
    }

}
