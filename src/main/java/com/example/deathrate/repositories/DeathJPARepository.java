package com.example.deathrate.repositories;

import com.example.deathrate.datamodel.DeathIDData;
import com.example.deathrate.datamodel.DeathRateData;
import org.springframework.data.repository.CrudRepository;

public interface DeathJPARepository extends CrudRepository<DeathRateData, DeathIDData> {
}
