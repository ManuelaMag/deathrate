package com.example.deathrate.repositories;

import com.example.deathrate.datamodel.DeathIDData;
import com.example.deathrate.datamodel.DeathRateData;
import com.example.deathrate.datamodel.mappers.DeathRateDataMapper;
import com.example.deathrate.model.DeathRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class DeathDBRepository implements DeathRepository {

    private DeathJPARepository jpaRepository;

    @Autowired
    public DeathDBRepository(DeathJPARepository jpaRepository) {
        this.jpaRepository = jpaRepository;
    }

    /**
     * Method to create and save a Death Rate into the database
     *
     * @param year       Year referring to death rate
     * @param country    Country Code referring to death rate
     * @param femaleRate value of female death rate
     * @param maleRate   value of male death rate
     * @return Created Death Rate
     */
    @Override
    public DeathRate createDeathRate(int year, String country, double femaleRate, double maleRate) {
        DeathRate newDeathRate = new DeathRate(year, country, femaleRate, maleRate);
        if (containsDeathRate(year, country))
            throw new IllegalStateException("This rate to year/country already exists");

        DeathRateData deathRateData = DeathRateDataMapper.toData(newDeathRate);
        DeathRateData deathRateDataSaved = jpaRepository.save(deathRateData);

        return DeathRateDataMapper.toDomain(deathRateDataSaved);

    }

    /**
     * Method to update a Death Rate into the database
     *
     * @param year       Year referring to death rate
     * @param country    Country Code referring to death rate
     * @param femaleRate new value of female death rate
     * @param maleRate   new value of male death rate
     * @return updated Death Rate
     */
    public DeathRate updateDeathRate(int year, String country, double femaleRate, double maleRate) {
        DeathIDData deathIDData = new DeathIDData();
        deathIDData.setYear(year);
        deathIDData.setCountry(country);

        DeathRate deathRate = new DeathRate(year, country, femaleRate, maleRate);

        Optional<DeathRateData> deathRateOptional = jpaRepository.findById(deathIDData);
        if (!deathRateOptional.isPresent()) throw new NullPointerException("This rate don't exist");

        DeathRateData deathRateData = deathRateOptional.get();
        deathRateData.setFemaleRate(femaleRate);
        deathRateData.setMaleRate(maleRate);

        DeathRateData deathRateDataUpdated = jpaRepository.save(deathRateData);


        return DeathRateDataMapper.toDomain(deathRateDataUpdated);
    }

    /**
     * @param year year of death rate to list
     * @return list with death rate of a year
     */
    @Override
    public List<DeathRate> findAllYearDeathRate(int year) {
        Iterable<DeathRateData> iterableData = jpaRepository.findAll();

        List<DeathRate> list = new ArrayList<>();
        for (DeathRateData unitIterableData : iterableData) {
            if (unitIterableData.getDeathID().getYear() == year)
                list.add(DeathRateDataMapper.toDomain(unitIterableData));
        }

        return list;
    }

    /**
     * Check if a death rate exists in the database
     *
     * @param year    Identification of the year
     * @param country Identification of the country
     * @return true death rate exists, false otherwise
     */
    private boolean containsDeathRate(int year, String country) {
        DeathIDData deathIDData = new DeathIDData();
        deathIDData.setYear(year);
        deathIDData.setCountry(country.toUpperCase());

        return jpaRepository.existsById(deathIDData);

    }

    public void deleteAll() {
        jpaRepository.deleteAll();
    }
}
