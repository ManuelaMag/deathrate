package com.example.deathrate.repositories;

import com.example.deathrate.model.DeathRate;

import java.util.List;

public interface DeathRepository {

    DeathRate createDeathRate(int year, String country, double femaleRate, double maleRate);

    DeathRate updateDeathRate(int year, String country, double femaleRate, double maleRate);

    List<DeathRate> findAllYearDeathRate(int year);

    void deleteAll();
}
